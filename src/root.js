import React, { Component } from 'react'
import { View, Text} from 'react-native'
import { Provider } from 'react-redux'

import store from './stores/store'
import { MainNavigator } from './routers/AppNavigator'

export default class Root extends Component {

  render() {
    return (
      <Provider store={store}>
        <MainNavigator />
      </Provider>
    );
  }

}
