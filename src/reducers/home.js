const initialState = {
    homeData: {
      data: [],
      state: 'No loading',
    },
   }
   
   
   /**
   * Home reducer
   * REDUCER MUST BE PURE AND
   * ALWAYS RETURN NEW STATE
   *
   * Notice that this function uses lot of new object creation syntax, spread. Works like:
   *
    ```*   const a = { a1: 1, a2: 2 }
    *   constole.log({ ...a, { b1: 3 } }) // -> { a1: 1, a2: 2, b1: 3 }
    *``` 
   *
   * @export
   * @param {Object} [state=initialState]
   * @param {Object} action
   * @returns {Object}
   */
   export default function (state = initialState, action) {
    switch (action.type) {
      case 'FETCH_HOME_DATA_REQUEST':
        return { ...initialState, homeData: { data: state.homeData.data, state: 'Loading' } }
   
      case 'FETCH_HOME_DATA_SUCCESS':
        return { ...state, homeData: { data: action.data.data, state: 'Success' } }
   
      case 'FETCH_HOME_DATA_ERROR':
        return { ...state, homeData: { data: state.homeData.data, state: 'Error' } }
   
      default: return state
    }
   }