import {combineReducers} from 'redux'

// import all reducers of our app
import drawer from './drawer'
import homeReducer from './home'

// This structure will be actual structure of our state.
const reducers = {
  home: homeReducer,
  drawer,

}

export default combineReducers(reducers)
