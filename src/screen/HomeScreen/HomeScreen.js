import React, { Component } from 'react'
import {View, Text } from 'react-native'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'

import types from '../../model/props-valiation'
import Button from '../../components/Button'
import LoadingView from '../../components/LoadingView'
import styles from './styles'
import { fetchHomeData } from '../../actions/home'
import { renderHomeIcon } from '../../components/svg-icon/svg-icons';

class HomeScreen extends Component {
    
    static navigationOptions = ({navigation}: Object): Object => ({
        tabBarIcon: renderHomeIcon,
      });

    componentWillMount() {
      }
    
    componentWillReceiveProps(nextProps) {  
      }

    onButtonPress() {
        this.props.getHomeData()
      }

    render() {
        const { homeData } = this.props.homeData;
        console.log(homeData);
        return (
            <View style={styles.layer1}>
                <View style={styles.sublayer}>
                    <Button onPress={() => this.onButtonPress()}>
                        Get Home Data
                    </Button>
                </View>
                <Text>{`${homeData.state}`}</Text>
                <Text style={styles.homeDataTextStyle}>{`${homeData.data.data}`}</Text>
            </View>
            );
    }
}

const mapStateToProps = (state) => ({
    homeData: state.home,
  })
  
  const mapDispatchToProps = (dispatch) => ({
    getHomeData: () => dispatch(fetchHomeData())
})

HomeScreen.propTypes = {
    homeData: types.homeDataModel,
    getHomeData: PropTypes.func
  }

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);